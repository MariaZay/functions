const getSum = (str1, str2) => {
  let validator = function(arg) {
      if (arg === '') {
        return BigInt(0);
      } else if (isNaN(Number(arg)) || arg.length === 0)  {
        return false;
      }
      return BigInt(arg);
  }

  if (!validator(str1) && str1 !== '' || !validator(str2) && str2 !== '') {
    return false;
  }
  return (validator(str1) + validator(str2)).toString();
}

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let result = {
   post: 0,
   comments: 0
  };
  for (let key of listOfPosts) {
   if (key.author === authorName && key.hasOwnProperty('post')) { 
     result.post++;
    }
   if (key.hasOwnProperty('comments')) {
      for (let item of key.comments) {
        if (item.author === authorName && item.hasOwnProperty('comment')) {   
          result.comments++;
        }
      }
    }    
  }
 return `Post:${result.post},comments:${result.comments}`;
}     


const tickets=(people)=> {
  let cashBills = people.map(item => Number(item));
  let cash = 0;
  let price = 25;
  for (let bill of cashBills) {
      if (bill !== price && ((cash + price) - bill < 0)) {
      return 'NO';
    } else {
      cash += price;
    } 
  }
  return 'YES';
}


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
